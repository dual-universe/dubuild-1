local currentPath = _ENV.arg[0]

local function getParentPath(path)
    pattern1 = "^(.+)/"
    pattern2 = "^(.+)\\"

    if (string.match(path,pattern1) == nil) then
        return string.match(path,pattern2)
    else
        return string.match(path,pattern1)
    end
end

local currentDir = getParentPath(currentPath)
local includeDir = ';' .. currentDir .. '/?.lua'
package.path = package.path .. includeDir

local json = require'dkjson'
local util = require'Util'
local Parser = require'ParseLua'
local Format_Mini = require'FormatMini'
local ArgParse = require'argparse'
local ParseLua = Parser.ParseLua

local function loadFile(fullPath)
    local inf = io.open(fullPath, 'r')
    if not inf then
		print("Failed to open `"..fullPath.."` for reading")
		return
	end
	--
	local text = inf:read('*all')
    inf:close()
    return text
end
local function loadJson(fullPath)
    local fileText = loadFile(fullPath)

    local obj, pos, err = json.decode (fileText, 1, nil)
    if err then
        print ("Json decode error :", err)
    else
        return obj
    end
end
local function minifyLua(luaText)
    local st, ast = ParseLua(luaText)
    if not st then 
        print("Error minifying lua :",st)
        return luaText
    end
    return Format_Mini(ast)
end
local function saveFile(txt, fullPath)
    local outf = io.open(fullPath, 'w')
	if not outf then
		print("Failed to open `"..fullPath.."` for writing")
		return
	end
    outf:write(txt)
    outf:close()
end
local function saveJson(object, fullPath)
    local json = json.encode(object)
    saveFile(json, fullPath)
end
local function shallowcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

local function generateUnencrypted(config, template)
    for i=1,#config.slots do
        --Load file
        local slotFileText = ""
        if (config.slots[i].file) then
            if type(config.slots[i].file) == "table" then
                for fi=1,#config.slots[i].file do
                    slotFileText = slotFileText .. loadFile(config.slots[i].file[fi])
                end
            else 
                slotFileText = loadFile(config.slots[i].file)
            end
        else
            slotFileText = config.slots[i].code
        end
    
        if config.minify then
            slotFileText = minifyLua(slotFileText)
        end

        local finalCode = ""
        if config.slots[i].config then
            finalCode = config.slots[i].config
        end
        if config.slots[i].configFile then
            finalCode = loadFile(config.slots[i].configFile)
        end

        local slotTable = {
            code = slotFileText,
            filter = {
                args = config.slots[i].args,
                signature = config.slots[i].signature,
                slotKey = config.slots[i].slot
            },
            key = i
        }
    
        table.insert(template.handlers, slotTable)
    end
    return template
end

local function generateOutput(config, template, outputPath, outputNameFormat, cryptOverride)
    --Add tag to identify commit version
    local commitSlot = {
        code = string.format("-- %s -- Commit : %s -- %s", os.getenv("CI_PROJECT_NAME"), os.getenv("CI_COMMIT_SHA"), os.getenv("GITLAB_USER_EMAIL")),
        filter = {
            args = {},
            signature = "start()",
            slotKey = "-2"
        },
        key = 98
    }
    
    local templateCopy = shallowcopy(template)
    templateCopy = generateUnencrypted(config, templateCopy)
    table.insert(template.handlers, commitSlot)
    local outputFileName = string.format(outputNameFormat, 0)
    local outputFilePath = string.format("%s/%s", outputPath, outputFileName)
    saveJson(templateCopy, outputFilePath)

end


local parser = ArgParse("dubuild", "Dual Universe script compiler")
parser:argument("config", "Build config file")
parser:argument("template", "Build template")
parser:argument("outputDir", "Directory to place output files in")
parser:option("-f --outFormat", "Name format for the output files", "out.json")

local args = parser:parse()

--[[local config = loadJson(_ENV.arg[1])
local template = loadJson(_ENV.arg[2])
local outputFile = _ENV.arg[3]

local config = loadJson("./test/Compiler_Config.crypt.json")
local template = loadJson("./template.json")
local outputFile = "outjson.json"
]]

local configJson = loadJson(args.config)
local templateJson = loadJson(args.template)

generateOutput(configJson, templateJson, args.outputDir, args.outFormat, args.crypt)

--[[
    Json layout :
    "minify":false,
    "constructID":1,
    "slots": [
        {
            -- File can be "" or []
            "file":"stec.json",
            "slot":"-3"
            "signature":"start()",
            "args":[],
            "encrypt":false
        }
    ]
]]