# Dual Universe ship config compiler with obfuscation and encryption
Rainfall - Shadow Templar

## Usage

A config is required, example config :
```
{
    "minify":false,
    "slots": [
        {
            "file":"src/STEC_config.lua",
            "slot":"-1",
            "signature":"start()",
            "args":[]
        },
        {
            "code":"system.showScreen(0)",
            "slot":"-1",
            "signature":"stop()",
            "args":[]
        },
        {
            "code":"keybindPresets[keybindPreset].Call(action, \"down\")",
            "slot":"-2",
            "signature":"actionStart(action)",
            "args":[{"variable":"*"}]
        },
        {
            "code":"keybindPresets[keybindPreset].Call(action, \"up\")",
            "slot":"-2",
            "signature":"actionStop(action)",
            "args":[{"variable":"*"}]
        },
        {
            "code":"keybindPresets[keybindPreset].Call(action, \"loop\")",
            "slot":"-2",
            "signature":"actionLoop(action)",
            "args":[{"variable":"*"}]
        },
        {
            "file":"src/TagManager.lua",
            "slot":"-3",
            "signature":"start()",
            "args":[]
        }
    ]
}
```

## Descriptions
- file : Load contents from a file, the file path can be relative to the current working directory or an absolute path
- code : Directly insert the code into the slot
- slot : Which slot to insert the code, Unit (-1), System (-2), Library (-3)
- signature : Which type of slot (or event) to put the code into, ie start(), stop(), flush(), update(), actionStart(action), actionStop(action), actionLoop(action)
- args : The slot arguments, mostly used for actionStart/Stop/Loop - for example '*' for all events, 'Forward' for the forward key press
- minify : If true, the code is passed through a lua minifier first

## Example Command
When in the working directory (ie the path from which the files into the config are referenced)
```
lua [compiler.lua] [path to config] [-f output file name] [path to template] [path to output files]
x:\Source> lua53.exe x:\dubuild\compiler.lua x:\source\config.json -f "example.json" x:\dubuild\template.json x:\output
```